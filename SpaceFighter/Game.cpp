#include "Game.h"
#include "MainMenuScreen.h"


namespace Game
{

	Game::Game()
	{
		SetResourceDirectory("..\\Sample\\Content\\");

		Font::SetLoadSize(18, true);

		Font* pFont = GetResourceManager()->Load<Font>("Fonts\\Arialbd.ttf", false);
		SetFrameCounterFont(pFont);
	}

	void Game::LoadContent(ResourceManager* pResourceManager)
	{
		LoadStaticResources(pResourceManager);

		GetScreenManager()->AddScreen(new MainMenuScreen());

		ResetGameTime();
	}

	void Game::LoadStaticResources(ResourceManager* pResourceManager)
	{
		Texture* pTexture;
		Animation* pAnimation;

		pAnimation = pResourceManager->Load<Animation>("Animations\\Explosion.anim");
		pAnimation->SetTexture(pResourceManager->Load<Texture>("Textures\\Explosion01.png"));
		pAnimation->Stop();
	}
}
