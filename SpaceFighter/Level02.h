#pragma once

#include "Level.h"

class Level02 : public Level
{

public:


	virtual ~Level02() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void UnloadContent() { }

	virtual int GetNextLevelIndex() { return 2; }

	

};



