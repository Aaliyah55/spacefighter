#pragma once
#include <string>
#include "GameplayScreen.h"
class Game  : public :: Game
{
public:

	Game();
	virtual ~Game() { }

	virtual std::string GetName() const { return "Sample Game"; }

	virtual void LoadContent(ResourceManager* pResourceManager);

private:

	virtual void LoadStaticResources(ResourceManager* pResourceManager);

};

