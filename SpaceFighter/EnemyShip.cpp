
#include "EnemyShip.h"
#include "Level.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	// Setting positions for the enemy ships
	SetPosition(position);

	// Delay seconds of when enemy ships show up
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	// Once the ship is hit, it takes damage and dies
	Ship::Hit(damage);
	if (!IsActive())
	{
		GetCurrentLevel()->SpawnExplosion(GetPosition(), 0.75f);
	}
}

void EnemyShip::Deactivate()
{
	if (IsLastEnemy())
	{
		GetCurrentLevel()->SetLevelComplete();
	}
	Ship::Deactivate();
}