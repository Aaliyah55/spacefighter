#pragma once

#include "Level.h"
#include "GameplayScreen.h"

class Level01 :	public Level
{

public:
	
	Level01() { }

	//Level01(GameplayScreen* pGameplayScreen);

	virtual ~Level01() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

	virtual int GetNextLevelIndex() { return 1; }

	

};

