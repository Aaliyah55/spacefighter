#pragma once
#include "KatanaEngine.h"


namespace KatanaEngine
{
	class Explosion
	{

	public:

		Explosion() { }
		virtual ~Explosion() { }

		virtual void Update(const GameTime* pGameTime);

		virtual void Draw(SpriteBatch* pSpriteBatch);

		virtual void SetAnimation(Animation* pAnimation) { m_pAnimation = pAnimation; }

		virtual void Activate(const Vector2 position, const float scale = 1);

		virtual bool IsActive() const { return m_pAnimation->IsPlaying(); }


	protected:

		virtual void SetPosition(const Vector2 position) { m_position = position; }


	private:

		Animation* m_pAnimation;

		Vector2 m_position;

		float m_rotation;
		float m_scale;

	};
}

